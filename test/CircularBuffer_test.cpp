//
// Copyright (C) 2021 by Morgan Camp
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///
/// @brief Unit test for circular buffer
///

//
// Project Includes
//
#include "CppUTest/TestHarness.h"

extern "C"
{
#include "CircularBuffer.h"
}

//
// System Includes
//
// N/A

//
// #defines, consts
//
// N/A

//
// typedefs, enums, structs
//
// N/A

//
// Local variables
//
const size_t cbufferSize = 20;
DEFINE_CBUF(cbufHandle, cbufHandleArr, cbufferSize);

//
// Function prototypes
//
// N/A

//
// Unit Tests
//
TEST_GROUP(group_CBUF_Init)
{
    TEST_SETUP()
    {
        cbufHandle.buffer = &cbufHandleArr[0];
        cbufHandle.capacity = cbufferSize;
        cbufHandle.firstIdx = 5;
        cbufHandle.lastIdx = 10;
        cbufHandle.overrunCount = 15;
        cbufHandle.full = true;
    }
};

TEST_GROUP(group_CBUF_SetEmpty)
{
    TEST_SETUP()
    {
        cbufHandle.buffer = &cbufHandleArr[0];
        cbufHandle.capacity = cbufferSize;
        cbufHandle.firstIdx = 5;
        cbufHandle.lastIdx = 10;
        cbufHandle.overrunCount = 15;
        cbufHandle.full = true;
    }
};

TEST_GROUP(group_CBUF_GetSize)
{
    TEST_SETUP()
    {
        cbufHandle.buffer = &cbufHandleArr[0];
        cbufHandle.capacity = cbufferSize;
        cbufHandle.firstIdx = 0;
        cbufHandle.lastIdx = 0;
        cbufHandle.overrunCount = 0;
        cbufHandle.full = false;
    }
};

TEST_GROUP(group_CBUF_GetAvailable)
{
    TEST_SETUP()
    {
        cbufHandle.buffer = &cbufHandleArr[0];
        cbufHandle.capacity = cbufferSize;
        cbufHandle.firstIdx = 0;
        cbufHandle.lastIdx = 0;
        cbufHandle.overrunCount = 0;
        cbufHandle.full = false;
    }
};

TEST_GROUP(group_CBUF_Append)
{
    TEST_SETUP()
    {
        cbufHandle.buffer = &cbufHandleArr[0];
        cbufHandle.capacity = cbufferSize;
        cbufHandle.firstIdx = 0;
        cbufHandle.lastIdx = 0;
        cbufHandle.overrunCount = 0;
        cbufHandle.full = false;
    }
};

TEST_GROUP(group_CBUF_Remove)
{
    TEST_SETUP()
    {
        cbufHandle.buffer = &cbufHandleArr[0];
        cbufHandle.capacity = cbufferSize;
        cbufHandle.firstIdx = 0;
        cbufHandle.lastIdx = 0;
        cbufHandle.overrunCount = 0;
        cbufHandle.full = false;
    }
};

TEST_GROUP(group_CBUF_AppendForce)
{
    TEST_SETUP()
    {
        cbufHandle.buffer = &cbufHandleArr[0];
        cbufHandle.capacity = cbufferSize;
        cbufHandle.firstIdx = 0;
        cbufHandle.lastIdx = 0;
        cbufHandle.overrunCount = 0;
        cbufHandle.full = false;
    }
};

TEST_GROUP(group_CBUF_RemoveForce)
{
    TEST_SETUP()
    {
        cbufHandle.buffer = &cbufHandleArr[0];
        cbufHandle.capacity = cbufferSize;
        cbufHandle.firstIdx = 0;
        cbufHandle.lastIdx = 0;
        cbufHandle.overrunCount = 0;
        cbufHandle.full = false;
    }
};

TEST_GROUP(group_CBUF_Peek)
{
    TEST_SETUP()
    {
        cbufHandle.buffer = &cbufHandleArr[0];
        cbufHandle.capacity = cbufferSize;
        cbufHandle.firstIdx = 0;
        cbufHandle.lastIdx = 0;
        cbufHandle.overrunCount = 0;
        cbufHandle.full = false;
    }
};

TEST(group_CBUF_Init, good_buffer)
{
    // Setup data
    const size_t bufferSize = 10;
    char cbufHandleArr2[bufferSize] = {0};

    // Call function under test
    bool result = CBUF_Init( &cbufHandle, &cbufHandleArr2[0], bufferSize );

    // Perform tests
    CHECK_EQUAL(&cbufHandleArr2[0], cbufHandle.buffer);
    CHECK_EQUAL(bufferSize, cbufHandle.capacity);
    CHECK_EQUAL(0, cbufHandle.firstIdx);
    CHECK_EQUAL(0, cbufHandle.lastIdx);
    CHECK_EQUAL(0, cbufHandle.overrunCount);
    CHECK_FALSE(cbufHandle.full);
    CHECK_TRUE(result);
}

TEST (group_CBUF_Init, assert_handle)
{
    // Setup data
    const size_t bufferSize = 10;
    char cbufHandleArr2[bufferSize] = {0};

    // Call function under test
    bool result = CBUF_Init( NULL, &cbufHandleArr2[0], bufferSize );

    // Perform tests
    CHECK_FALSE(result);
}

TEST (group_CBUF_Init, assert_array)
{
    // Setup data
    // N/A

    // Call function under test
    bool result = CBUF_Init( &cbufHandle, NULL, cbufferSize );

    // Perform tests
    CHECK_FALSE(result);
}

TEST (group_CBUF_Init, assert_bufferSize)
{
    // Setup data
    // N/A

    // Call function under test
    bool result = CBUF_Init( &cbufHandle, &cbufHandleArr[0], 0 );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_CBUF_SetEmpty, good_handle)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    char* expectedBuffer = cbufHandle.buffer;

    // Call function under test
    bool result = CBUF_SetEmpty( &cbufHandle );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(0, cbufHandle.firstIdx);
    CHECK_EQUAL(0, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_FALSE(cbufHandle.full);
    CHECK_TRUE(result);
}

TEST(group_CBUF_SetEmpty, assert_handle)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;

    // Call function under test
    bool result = CBUF_SetEmpty( NULL );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_GetSize, good_empty)
{
    // Setup data
    size_t expectedSize = 0;

    // Fill buffer / Empty buffer:
    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        char removedData = 0;
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);

        bool removeRes = CBUF_Remove( &cbufHandle, &removedData, 1);
        CHECK_TRUE(removeRes);
        CHECK_EQUAL(removedData, '4');
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetSize( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL(expectedSize, size);
}


TEST(group_CBUF_GetSize, good_full)
{
    // Setup data
    size_t expectedSize = 0;

    // Fill buffer:
    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);
        expectedSize++;
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetSize( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL(expectedSize, size);
}

TEST(group_CBUF_GetSize, good_overlap)
{
    // Setup data
    size_t expectedSize = 0;
    cbufHandle.firstIdx = 10;
    cbufHandle.lastIdx = 10;

    // Fill buffer:
    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);
        expectedSize++;
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetSize( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL(expectedSize, size);
}

TEST(group_CBUF_GetSize, good_first_lt_last)
{
    // Setup data
    size_t expectedSize = 0;
    cbufHandle.firstIdx = cbufHandle.capacity - 10;
    cbufHandle.lastIdx = cbufHandle.capacity - 10;

    // Fill buffer:
    for( size_t i = 0; i < 10; i++ )
    {
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);
        expectedSize++;
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetSize( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL(expectedSize, size);
    CHECK_EQUAL(0, cbufHandle.lastIdx);
    CHECK_EQUAL(cbufHandle.capacity - 10, cbufHandle.firstIdx);
}

TEST(group_CBUF_GetSize, good_first_gt_last)
{
    // Setup data
    size_t expectedSize = 0;
    cbufHandle.firstIdx = cbufHandle.capacity - 10;
    cbufHandle.lastIdx = cbufHandle.capacity - 10;

    // Fill buffer:
    for( size_t i = 0; i < 9; i++ )
    {
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);
        expectedSize++;
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetSize( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL(expectedSize, size);
    CHECK_EQUAL(cbufHandle.capacity - 1, cbufHandle.lastIdx);
    CHECK_EQUAL(cbufHandle.capacity - 10, cbufHandle.firstIdx);
}

TEST(group_CBUF_GetSize, assert_handle)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetSize( NULL, &size );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_GetSize, assert_size)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;

    // Call function under test
    bool result = CBUF_GetSize( &cbufHandle, NULL );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_GetAvailable, good_empty)
{
    // Setup data
    size_t expectedSize = 0;

    // Fill buffer / Empty buffer:
    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        char removedCh = 0;
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);

        bool removeRes = CBUF_Remove( &cbufHandle, &removedCh, 1);
        CHECK_TRUE(removeRes);
        CHECK_EQUAL(removedCh, '4');
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetAvailable( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL( (cbufHandle.capacity - expectedSize), size);
}

TEST(group_CBUF_GetAvailable, good_full)
{
    // Setup data
    size_t expectedSize = 0;

    // Fill buffer:
    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);
        expectedSize++;
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetAvailable( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL( (cbufHandle.capacity - expectedSize), size);
}



TEST(group_CBUF_GetAvailable, good_overlap)
{
    // Setup data
    size_t expectedSize = 0;
    cbufHandle.firstIdx = 10;
    cbufHandle.lastIdx = 10;

    // Fill buffer:
    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);
        expectedSize++;
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetAvailable( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL( (cbufHandle.capacity - expectedSize), size);
}

TEST(group_CBUF_GetAvailable, good_first_lt_last)
{
    // Setup data
    size_t expectedSize = 0;
    cbufHandle.firstIdx = cbufHandle.capacity - 10;
    cbufHandle.lastIdx = cbufHandle.capacity - 10;

    // Fill buffer:
    for( size_t i = 0; i < 10; i++ )
    {
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);
        expectedSize++;
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetAvailable( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL( (cbufHandle.capacity - expectedSize), size);
    CHECK_EQUAL(0, cbufHandle.lastIdx);
    CHECK_EQUAL(cbufHandle.capacity - 10, cbufHandle.firstIdx);
}

TEST(group_CBUF_GetAvailable, good_first_gt_last)
{
    // Setup data
    size_t expectedSize = 0;
    cbufHandle.firstIdx = cbufHandle.capacity - 10;
    cbufHandle.lastIdx = cbufHandle.capacity - 10;

    // Fill buffer:
    for( size_t i = 0; i < 9; i++ )
    {
        char appendCh = '4';
        bool appendRes = CBUF_Append( &cbufHandle, &appendCh, 1 );
        CHECK_TRUE(appendRes);
        expectedSize++;
    }

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetAvailable( &cbufHandle, &size );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL( (cbufHandle.capacity - expectedSize), size);
    CHECK_EQUAL(cbufHandle.capacity - 1, cbufHandle.lastIdx);
    CHECK_EQUAL(cbufHandle.capacity - 10, cbufHandle.firstIdx);
}

TEST(group_CBUF_GetAvailable, assert_handle)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;

    // Call function under test
    size_t size = 0;
    bool result = CBUF_GetAvailable( NULL, &size );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_GetAvailable, assert_size)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;

    // Call function under test
    bool result = CBUF_GetAvailable( &cbufHandle, NULL );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Append, good_single)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }

    // Call function under test
    bool appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 1);
    CHECK_TRUE(appendRes);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 1, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 1 );


    for( size_t i = 0; i < 1; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i]);
    }
}

TEST(group_CBUF_Append, good_fill)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }

    // Call function under test
    bool appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 20);
    CHECK_TRUE(appendRes);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 0 );


    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i]);
    }
}

TEST(group_CBUF_Append, good_fill_overlap)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }

    cbufHandle.firstIdx = 10;
    cbufHandle.lastIdx = 10;

    // Call function under test
    bool appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 20);
    CHECK_TRUE(appendRes);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 10 );


    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        if( i < 10 )
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i + 10]);
        }
        else
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i - 10]);
        }
    }
}

TEST(group_CBUF_Append, fail_append_while_full)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    cbufHandle.full = true;

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
        cbufHandle.buffer[i] = (char) 21;
    }

    // Call function under test
    bool appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 20);
    CHECK_FALSE(appendRes);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 0 );

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], 21);
    }
}

TEST(group_CBUF_Append, assert_handle)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};


    // Call function under test
    bool result = CBUF_Append( NULL, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Append, assert_handle_buffer)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = NULL;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    cbufHandle.buffer = NULL;

    // Call function under test
    bool result = CBUF_Append( &cbufHandle, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Append, assert_inputarr)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;


    // Call function under test
    bool result = CBUF_Append( &cbufHandle, NULL, 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Append, assert_inputsize)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    // Call function under test
    bool result = CBUF_Append( &cbufHandle, &memToCopy[0], 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Append, assert_inputsize_overflow)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[21] = {0};

    // Call function under test
    bool result = CBUF_Append( &cbufHandle, &memToCopy[0], 21 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}


TEST(group_CBUF_Remove, good_single)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};
    char memToCopy2[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }
    bool appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 1);
    CHECK_TRUE(appendRes);

    // Call function under test
    bool removeRes = CBUF_Remove( &cbufHandle, &memToCopy2[0], 1);
    CHECK_TRUE(removeRes);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.firstIdx, 1 );

    for( size_t i = 0; i < 1; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy2[i]);
    }
}

TEST(group_CBUF_Remove, good_full)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};
    char memToCopy2[20] = {0};

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        memToCopy[i] = (char) i;
    }
    bool appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], cbufHandle.capacity);
    CHECK_TRUE(appendRes);

    // Call function under test
    bool removeRes = CBUF_Remove( &cbufHandle, &memToCopy2[0], cbufHandle.capacity);
    CHECK_TRUE(removeRes);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.firstIdx, 0 );

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy2[i]);
    }
}

TEST(group_CBUF_Remove, good_full_overlap)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        cbufHandle.buffer[i] = (char) i;
    }

    cbufHandle.full = true;
    cbufHandle.firstIdx = 10;
    cbufHandle.lastIdx = 10;

    // Call function under test
    bool removeRes = CBUF_Remove( &cbufHandle, &memToCopy[0], 20);
    CHECK_TRUE(removeRes);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 10 );


    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        if( i < 10 )
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i + 10]);
        }
        else
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i - 10]);
        }
    }
}

TEST(group_CBUF_Remove, fail_remove_while_empty)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    cbufHandle.full = false;

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
        cbufHandle.buffer[i] = (char) 21;
    }

    // Call function under test
    bool removeRes = CBUF_Remove( &cbufHandle, &memToCopy[0], 20);
    CHECK_FALSE(removeRes);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 0 );

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], 21);
    }
}

TEST(group_CBUF_Remove, assert_handle)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};


    // Call function under test
    bool result = CBUF_Remove( NULL, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Remove, assert_handle_buffer)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = NULL;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    cbufHandle.buffer = NULL;

    // Call function under test
    bool result = CBUF_Remove( &cbufHandle, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Remove, assert_inputarr)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;


    // Call function under test
    bool result = CBUF_Remove( &cbufHandle, NULL, 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Remove, assert_inputsize)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    // Call function under test
    bool result = CBUF_Remove( &cbufHandle, &memToCopy[0], 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Remove, assert_inputsize_overflow)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[21] = {0};

    // Call function under test
    bool result = CBUF_Remove( &cbufHandle, &memToCopy[0], 21 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Remove, assert_remove_while_empty)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[21] = {0};

    // Call function under test
    bool result = CBUF_Remove( &cbufHandle, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_Remove, assert_remove_too_many)
{
    // Setup data
    cbufHandle.lastIdx = 5;

    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[21] = {0};

    // Call function under test
    bool result = CBUF_Remove( &cbufHandle, &memToCopy[0], 6 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_FALSE(result);
}

TEST(group_CBUF_AppendForce, good_single)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }

    // Call function under test
    bool appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 1);
    CHECK_TRUE(appendRes);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 1, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 1 );


    for( size_t i = 0; i < 1; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i]);
    }
}

TEST(group_CBUF_AppendForce, good_fill)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }

    // Call function under test
    size_t result = CBUF_AppendForce( &cbufHandle, &memToCopy[0], 20);
    CHECK_EQUAL(result, 20);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 0 );


    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i]);
    }
}

TEST(group_CBUF_AppendForce, good_fill_overlap)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }

    cbufHandle.firstIdx = 10;
    cbufHandle.lastIdx = 10;

    // Call function under test
    size_t result = CBUF_AppendForce( &cbufHandle, &memToCopy[0], 20);
    CHECK_EQUAL(result, 20);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 10 );


    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        if( i < 10 )
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i + 10]);
        }
        else
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i - 10]);
        }
    }
}

TEST(group_CBUF_AppendForce, good_fill_overflow)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }

    cbufHandle.firstIdx = 10;
    cbufHandle.lastIdx = 10;

    // Call function under test
    size_t result = CBUF_AppendForce( &cbufHandle, &memToCopy[0], 21);
    CHECK_EQUAL(result, 20);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 10 );


    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        if( i < 10 )
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i + 10]);
        }
        else
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i - 10]);
        }
    }
}

TEST(group_CBUF_AppendForce, good_append_while_full)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    cbufHandle.full = true;

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
        cbufHandle.buffer[i] = (char) 21;
    }

    // Call function under test
    size_t result = CBUF_AppendForce( &cbufHandle, &memToCopy[0], 20);
    CHECK_EQUAL(result, 20);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 0 );

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i]);
    }
}

TEST(group_CBUF_AppendForce, assert_handle)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};


    // Call function under test
    size_t result = CBUF_AppendForce( NULL, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_AppendForce, assert_handle_buffer)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = NULL;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    cbufHandle.buffer = NULL;

    // Call function under test
    size_t result = CBUF_AppendForce( &cbufHandle, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_AppendForce, assert_inputarr)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;


    // Call function under test
    size_t result = CBUF_AppendForce( &cbufHandle, NULL, 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_AppendForce, assert_inputsize)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    // Call function under test
    size_t result = CBUF_AppendForce( &cbufHandle, &memToCopy[0], 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_RemoveForce, good_single)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};
    char memToCopy2[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }
    bool appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 1);
    CHECK_TRUE(appendRes);

    // Call function under test
    size_t removeRes = CBUF_RemoveForce( &cbufHandle, &memToCopy2[0], 1);
    CHECK_EQUAL(removeRes, 1);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.firstIdx, 1 );

    for( size_t i = 0; i < 1; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy2[i]);
    }
}

TEST(group_CBUF_RemoveForce, good_full)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};
    char memToCopy2[20] = {0};

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        memToCopy[i] = (char) i;
    }
    size_t appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], cbufHandle.capacity);
    CHECK_TRUE(appendRes);

    // Call function under test
    size_t removeRes = CBUF_RemoveForce( &cbufHandle, &memToCopy2[0], cbufHandle.capacity);
    CHECK_EQUAL(removeRes, cbufHandle.capacity);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.firstIdx, 0 );

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy2[i]);
    }
}

TEST(group_CBUF_RemoveForce, good_full_overlap)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        cbufHandle.buffer[i] = (char) i;
    }

    cbufHandle.full = true;
    cbufHandle.firstIdx = 10;
    cbufHandle.lastIdx = 10;

    // Call function under test
    size_t removeRes = CBUF_RemoveForce( &cbufHandle, &memToCopy[0], 20);
    CHECK_EQUAL(removeRes, 20);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 10 );


    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        if( i < 10 )
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i + 10]);
        }
        else
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i - 10]);
        }
    }
}

TEST(group_CBUF_RemoveForce, fail_remove_while_empty)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    cbufHandle.full = false;

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
        cbufHandle.buffer[i] = (char) 21;
    }

    // Call function under test
    size_t removeRes = CBUF_RemoveForce( &cbufHandle, &memToCopy[0], 20);
    CHECK_EQUAL(removeRes, 0);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 0 );

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], 21);
    }
}

TEST(group_CBUF_RemoveForce, assert_handle)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};


    // Call function under test
    size_t result = CBUF_RemoveForce( NULL, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_RemoveForce, assert_handle_buffer)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = NULL;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    cbufHandle.buffer = NULL;

    // Call function under test
    size_t result = CBUF_RemoveForce( &cbufHandle, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_RemoveForce, assert_inputarr)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;


    // Call function under test
    size_t result = CBUF_RemoveForce( &cbufHandle, NULL, 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_RemoveForce, assert_inputsize)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    // Call function under test
    size_t result = CBUF_RemoveForce( &cbufHandle, &memToCopy[0], 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_RemoveForce, assert_inputsize_overflow)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[21] = {0};

    // Call function under test
    size_t result = CBUF_RemoveForce( &cbufHandle, &memToCopy[0], 21 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_RemoveForce, assert_remove_while_empty)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[21] = {0};

    // Call function under test
    size_t result = CBUF_RemoveForce( &cbufHandle, &memToCopy[0], 1 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_RemoveForce, good_remove_too_many)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};
    char memToCopy2[20] = {0};

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        memToCopy[i] = (char) i;
    }
    size_t appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 10);
    CHECK_TRUE(appendRes);

    // Call function under test
    size_t removeRes = CBUF_RemoveForce( &cbufHandle, &memToCopy2[0], cbufHandle.capacity);
    CHECK_EQUAL(removeRes, 10);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.firstIdx, 10 );

    for( size_t i = 0; i < 10; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy2[i]);
    }
}

TEST(group_CBUF_Peek, good_single_no_offset)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};
    char memToCopy2[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
    }
    bool appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 1);
    CHECK_TRUE(appendRes);

    // Call function under test
    size_t removeRes = CBUF_Peek( &cbufHandle, &memToCopy2[0], 1, 0);
    CHECK_EQUAL(removeRes, 1);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 1, size );
    CHECK_EQUAL( cbufHandle.firstIdx, 0 );
    CHECK_EQUAL( cbufHandle.lastIdx, 1 );

    for( size_t i = 0; i < 1; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy2[i]);
    }
}

TEST(group_CBUF_Peek, good_full_no_offset)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};
    char memToCopy2[20] = {0};

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        memToCopy[i] = (char) i;
    }
    size_t appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], cbufHandle.capacity);
    CHECK_TRUE(appendRes);

    // Call function under test
    size_t removeRes = CBUF_Peek( &cbufHandle, &memToCopy2[0], cbufHandle.capacity, 0);
    CHECK_EQUAL(removeRes, cbufHandle.capacity);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.firstIdx, 0 );

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy2[i]);
    }
}

TEST(group_CBUF_Peek, good_full_half_offset)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};
    char memToCopy2[20] = {0};

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        memToCopy[i] = (char) i;
    }
    size_t appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], cbufHandle.capacity);
    CHECK_TRUE(appendRes);

    // Call function under test
    size_t removeRes = CBUF_Peek( &cbufHandle, &memToCopy2[0], cbufHandle.capacity, 10);
    CHECK_EQUAL(removeRes, 10);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.firstIdx, 0 );

    for( size_t i = 0; i < 10; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i + 10], memToCopy2[i]);
    }
}

TEST(group_CBUF_Peek, good_full_overlap_no_offset)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    for( size_t i = 0; i < 20; i++ )
    {
        cbufHandle.buffer[i] = (char) i;
    }

    cbufHandle.full = true;
    cbufHandle.firstIdx = 10;
    cbufHandle.lastIdx = 10;

    // Call function under test
    size_t removeRes = CBUF_Peek( &cbufHandle, &memToCopy[0], 20, 0);
    CHECK_EQUAL(removeRes, 20);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( cbufHandle.capacity, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 10 );


    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        if( i < 10 )
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i + 10]);
        }
        else
        {
            CHECK_EQUAL(cbufHandle.buffer[i], memToCopy[i - 10]);
        }
    }
}

TEST(group_CBUF_Peek, fail_peek_while_empty)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};

    cbufHandle.full = false;

    for( size_t i = 0; i < 20; i++ )
    {
        memToCopy[i] = (char) i;
        cbufHandle.buffer[i] = (char) 21;
    }

    // Call function under test
    size_t removeRes = CBUF_Peek( &cbufHandle, &memToCopy[0], 20, 0);
    CHECK_EQUAL(removeRes, 0);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 0, size );
    CHECK_EQUAL( cbufHandle.lastIdx, 0 );

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], 21);
    }
}

TEST(group_CBUF_Peek, assert_handle)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};


    // Call function under test
    size_t result = CBUF_Peek( NULL, &memToCopy[0], 1, 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_Peek, assert_handle_buffer)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = NULL;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    cbufHandle.buffer = NULL;

    // Call function under test
    size_t result = CBUF_Peek( &cbufHandle, &memToCopy[0], 1, 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_Peek, assert_inputarr)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;


    // Call function under test
    size_t result = CBUF_Peek( &cbufHandle, NULL, 1, 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_Peek, assert_inputsize)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[20] = {0};

    // Call function under test
    size_t result = CBUF_Peek( &cbufHandle, &memToCopy[0], 0, 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_Peek, assert_inputsize_overflow)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[21] = {0};

    // Call function under test
    size_t result = CBUF_Peek( &cbufHandle, &memToCopy[0], 21, 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_Peek, assert_remove_while_empty)
{
    // Setup data
    size_t expectedOverrunCount = cbufHandle.overrunCount;
    size_t expectedCapacity = cbufHandle.capacity;
    size_t expectedFirstIdx = cbufHandle.firstIdx;
    size_t expectedLastIdx = cbufHandle.lastIdx;
    char* expectedBuffer = cbufHandle.buffer;
    bool expectedFull = cbufHandle.full;
    char memToCopy[21] = {0};

    // Call function under test
    size_t result = CBUF_Peek( &cbufHandle, &memToCopy[0], 1, 0 );

    // Perform tests
    CHECK_EQUAL(expectedBuffer, cbufHandle.buffer);
    CHECK_EQUAL(expectedCapacity, cbufHandle.capacity);
    CHECK_EQUAL(expectedFirstIdx, cbufHandle.firstIdx);
    CHECK_EQUAL(expectedLastIdx, cbufHandle.lastIdx);
    CHECK_EQUAL(expectedOverrunCount, cbufHandle.overrunCount);
    CHECK_EQUAL(expectedFull, cbufHandle.full);
    CHECK_EQUAL(result, 0);
}

TEST(group_CBUF_Peek, good_peek_too_many)
{
    // Setup data
    size_t size = 0;
    bool sizeResult = false;
    char memToCopy[20] = {0};
    char memToCopy2[20] = {0};

    for( size_t i = 0; i < cbufHandle.capacity; i++ )
    {
        memToCopy[i] = (char) i;
    }
    size_t appendRes = CBUF_Append( &cbufHandle, &memToCopy[0], 10);
    CHECK_TRUE(appendRes);

    // Call function under test
    size_t removeRes = CBUF_Peek( &cbufHandle, &memToCopy2[0], 21, 0 );
    CHECK_EQUAL(removeRes, 10);

    // Perform tests
    sizeResult = CBUF_GetSize( &cbufHandle, &size );
    CHECK_TRUE(sizeResult);
    CHECK_EQUAL( 10, size );
    CHECK_EQUAL( cbufHandle.firstIdx, 0 );

    for( size_t i = 0; i < 10; i++ )
    {
        CHECK_EQUAL(cbufHandle.buffer[i], memToCopy2[i]);
    }
}
