//
// Copyright (C) 2021 by Morgan Camp
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///
/// @brief Unit test for circular buffer
///

//
// Project Includes
//
#include "CppUTest/TestHarness.h"

extern "C"
{
#include "GenericKeyList.h"
}

//
// System Includes
//
// N/A

//
// #defines, consts
//
// N/A

//
// typedefs, enums, structs
//
typedef struct
{
    bool testBool;
    float testFloat;
    int testInt;
} TEST_DATA_T;

//
// Local variables
//
const size_t cbufferSize = 13;
DEFINE_GEN_KEY_LIST(gkListHandle, TEST_DATA_T, 13);

//
// Function prototypes
//
// N/A

//
// Unit Tests
//
TEST_GROUP(group_GKLIST_Insert)
{
    TEST_SETUP()
    {
        gkListHandle.values = &gkListHandle_Arr[0];
        gkListHandle.totalElementsStored = 0;
        gkListHandle.freedKeys = &gkListHandle_Cbuf;
        gkListHandle.keyUsedLUT = &gkListHandle_KeyUsedLUT[0];

        for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
        {
            gkListHandle.keyUsedLUT[i] = false;
        }
        CBUF_SetEmpty( gkListHandle.freedKeys );
    }
};

TEST_GROUP(group_GKLIST_Remove)
{
    TEST_SETUP()
    {
        gkListHandle.values = &gkListHandle_Arr[0];
        gkListHandle.totalElementsStored = 0;
        gkListHandle.freedKeys = &gkListHandle_Cbuf;
        gkListHandle.keyUsedLUT = &gkListHandle_KeyUsedLUT[0];

        for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
        {
            gkListHandle.keyUsedLUT[i] = false;
        }
        CBUF_SetEmpty( gkListHandle.freedKeys );
    }
};

TEST_GROUP(group_GKLIST_Copy)
{
    TEST_SETUP()
    {
        gkListHandle.values = &gkListHandle_Arr[0];
        gkListHandle.totalElementsStored = 0;
        gkListHandle.freedKeys = &gkListHandle_Cbuf;
        gkListHandle.keyUsedLUT = &gkListHandle_KeyUsedLUT[0];

        for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
        {
            gkListHandle.keyUsedLUT[i] = false;
        }
        CBUF_SetEmpty( gkListHandle.freedKeys );
    }
};

TEST_GROUP(group_GKLIST_GetRef)
{
    TEST_SETUP()
    {
        gkListHandle.values = &gkListHandle_Arr[0];
        gkListHandle.totalElementsStored = 0;
        gkListHandle.freedKeys = &gkListHandle_Cbuf;
        gkListHandle.keyUsedLUT = &gkListHandle_KeyUsedLUT[0];

        for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
        {
            gkListHandle.keyUsedLUT[i] = false;
        }
        CBUF_SetEmpty( gkListHandle.freedKeys );
    }
};

TEST(group_GKLIST_Insert, good_insert_until_full)
{
    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        // Setup data
        TEST_DATA_T testData = { true, (float) i, (int) i };
        size_t keyUsed = 12345;

        // Call function under test
        bool result = GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );

        // Perform tests
        CHECK_TRUE(result);
        CHECK_EQUAL(keyUsed, i);
        CHECK_TRUE(gkListHandle.keyUsedLUT[i]);
        CHECK_EQUAL(gkListHandle.totalElementsStored, i + 1);
    }
}

TEST(group_GKLIST_Insert, good_insert_middle)
{
    // Setup data
    size_t unusedKey = 10;
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;

    gkListHandle.totalElementsStored = gkListHandle.valueListCapacityElements - 1;
    CBUF_Append(gkListHandle.freedKeys, (char*) &unusedKey, sizeof(unusedKey) );

    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        gkListHandle.keyUsedLUT[i] = false;
    }
    gkListHandle.keyUsedLUT[unusedKey] = true;

    // Call function under test
    bool result = GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_EQUAL(keyUsed, unusedKey);
    CHECK_TRUE(gkListHandle.keyUsedLUT[keyUsed]);
    CHECK_EQUAL(gkListHandle.totalElementsStored, gkListHandle.valueListCapacityElements);
}

TEST(group_GKLIST_Insert, bad_insert_full)
{
    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        // Setup data
        TEST_DATA_T testData = { true, (float) i, (int) i };
        size_t keyUsed = 12345;

        // Call function under test
        bool result = GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );

        // Perform tests
        CHECK_TRUE(result);
        CHECK_EQUAL(keyUsed, i);
        CHECK_EQUAL(gkListHandle.totalElementsStored, i + 1);
    }

    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;

    // Call function under test
    bool result = GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(gkListHandle.totalElementsStored, gkListHandle.valueListCapacityElements);
}

TEST(group_GKLIST_Insert, bad_listHandle)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;

    // Call function under test
    bool result = GKLIST_Insert( NULL, (char *) &testData, &keyUsed );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(gkListHandle.totalElementsStored, 0);
}

TEST(group_GKLIST_Insert, bad_listHandleValues)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;
    gkListHandle.values = NULL;

    // Call function under test
    bool result = GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(gkListHandle.totalElementsStored, 0);
}

TEST(group_GKLIST_Insert, bad_keyLut)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;
    gkListHandle.keyUsedLUT = NULL;

    // Call function under test
    bool result = GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(gkListHandle.totalElementsStored, 0);
}

TEST(group_GKLIST_Insert, bad_data)
{
    // Setup data
    size_t keyUsed = 12345;

    // Call function under test
    bool result = GKLIST_Insert( &gkListHandle, NULL, &keyUsed );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(gkListHandle.totalElementsStored, 0);
}

TEST(group_GKLIST_Insert, bad_key)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };

    // Call function under test
    bool result = GKLIST_Insert( &gkListHandle, (char *) &testData, NULL );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(gkListHandle.totalElementsStored, 0);
}

TEST(group_GKLIST_Remove, good_remove_full)
{
    // Test setup
    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        // Setup data
        TEST_DATA_T testData = { true, (float) i, (int) i };
        size_t keyUsed = 12345;

        (void) GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );
    }

    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        // Setup data
        // N/A

        // Call function under test
        bool result = GKLIST_Remove( &gkListHandle, i );

        // Perform tests
        CHECK_TRUE(result);
        CHECK_EQUAL(gkListHandle.totalElementsStored, gkListHandle.valueListCapacityElements - i - 1);
    }
}

TEST(group_GKLIST_Remove, good_remove_middle)
{
    // Setup data
    size_t unusedKey = 10;

    gkListHandle.totalElementsStored = gkListHandle.valueListCapacityElements - 1;

    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        gkListHandle.keyUsedLUT[i] = false;
    }
    gkListHandle.keyUsedLUT[unusedKey] = true;

    // Call function under test
    bool result = GKLIST_Remove( &gkListHandle, unusedKey );

    // Perform tests
    CHECK_TRUE(result);
    CHECK_FALSE(gkListHandle.keyUsedLUT[unusedKey]);
    CHECK_EQUAL(gkListHandle.totalElementsStored, 11 );
}

TEST(group_GKLIST_Remove, bad_remove_empty)
{
    // Setup data
    // N/A

    // Call function under test
    bool result = GKLIST_Remove( &gkListHandle, 0 );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(gkListHandle.totalElementsStored, 0);
}

TEST(group_GKLIST_Remove, bad_listHandle)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;
    (void) GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );

    // Call function under test
    bool result = GKLIST_Remove( &gkListHandle, 1);

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GKLIST_Remove, bad_keyLut)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;
    (void) GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );
    gkListHandle.keyUsedLUT = NULL;

    // Call function under test
    bool result = GKLIST_Remove( &gkListHandle, 0 );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(gkListHandle.totalElementsStored, 1);
}

TEST(group_GKLIST_Remove, bad_key)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;
    (void) GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );
    gkListHandle.keyUsedLUT = NULL;

    // Call function under test
    bool result = GKLIST_Remove( &gkListHandle, 1 );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(gkListHandle.totalElementsStored, 1);
}

TEST(group_GKLIST_Copy, good_copy_all)
{
    // Setup data
    bool lastBool = true;
    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        lastBool = !lastBool;
        TEST_DATA_T testData = { lastBool, (float) i, (int) i };
        size_t keyUsed = 12345;

        // Call function under test
        (void) GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );
    }

    lastBool = true;
    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        lastBool = !lastBool;
        TEST_DATA_T testData = { 0 };

        // Call function under test
        bool result = GKLIST_Copy( &gkListHandle, (size_t) i, (char *) &testData );

        // Perform tests
        CHECK_TRUE(result);
        CHECK_EQUAL(testData.testBool, lastBool);
        CHECK_EQUAL(testData.testFloat, (float) i);
        CHECK_EQUAL(testData.testInt, (int) i);
    }
}

TEST(group_GKLIST_Copy, bad_listHandle)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;

    // Call function under test
    bool result = GKLIST_Copy( NULL, keyUsed, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GKLIST_Copy, bad_listHandleValues)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;
    gkListHandle.values = NULL;

    // Call function under test
    bool result = GKLIST_Copy( &gkListHandle, keyUsed, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GKLIST_Copy, bad_keyLut)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;
    gkListHandle.keyUsedLUT = NULL;

    // Call function under test
    bool result = GKLIST_Copy( &gkListHandle, keyUsed, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GKLIST_Copy, bad_data)
{
    // Setup data
    size_t keyUsed = 12345;

    // Call function under test
    bool result = GKLIST_Copy( &gkListHandle, keyUsed, NULL );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GKLIST_Copy, bad_key)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };

    // Call function under test
    bool result = GKLIST_Copy( &gkListHandle, gkListHandle.valueListCapacityElements, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GKLIST_GetRef, good_get_ref_all)
{
    // Setup data
    bool lastBool = true;
    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        lastBool = !lastBool;
        TEST_DATA_T testData = { lastBool, (float) i, (int) i };
        size_t keyUsed = 12345;

        // Call function under test
        (void) GKLIST_Insert( &gkListHandle, (char *) &testData, &keyUsed );
    }

    lastBool = true;
    for( size_t i = 0; i < gkListHandle.valueListCapacityElements; i++ )
    {
        lastBool = !lastBool;
        TEST_DATA_T testData = {0};
        TEST_DATA_T *testDataPtr = &testData;

        // Call function under test
        bool result = GKLIST_GetRef( &gkListHandle, (size_t) i, (char**) &testDataPtr );

        // Perform tests
        CHECK_TRUE(result);

        CHECK_EQUAL(testData.testBool, 0);
        CHECK_EQUAL(testData.testFloat, 0);
        CHECK_EQUAL(testData.testInt, 0);

        CHECK_EQUAL(testDataPtr->testBool, lastBool);
        CHECK_EQUAL(testDataPtr->testFloat, (float) i);
        CHECK_EQUAL(testDataPtr->testInt, (int) i);
    }
}

TEST(group_GKLIST_GetRef, bad_listHandle)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t keyUsed = 12345;
    TEST_DATA_T *testDataPtr = &testData;

    // Call function under test
    bool result = GKLIST_GetRef( NULL, keyUsed, (char **) &testDataPtr );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(&testData, testDataPtr);
}

TEST(group_GKLIST_GetRef, bad_listHandleValues)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    TEST_DATA_T *testDataPtr = &testData;
    size_t keyUsed = 12345;
    gkListHandle.values = NULL;

    // Call function under test
    bool result = GKLIST_GetRef( &gkListHandle, keyUsed, (char **) &testDataPtr );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(testDataPtr, &testData);
}

TEST(group_GKLIST_GetRef, bad_keyLut)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    TEST_DATA_T *testDataPtr = &testData;
    size_t keyUsed = 12345;
    gkListHandle.keyUsedLUT = NULL;

    // Call function under test
    bool result = GKLIST_GetRef( &gkListHandle, keyUsed, (char **) &testDataPtr );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(testDataPtr, &testData);
}

TEST(group_GKLIST_GetRef, bad_data)
{
    // Setup data
    size_t keyUsed = 12345;

    // Call function under test
    bool result = GKLIST_GetRef( &gkListHandle, keyUsed, NULL );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GKLIST_GetRef, bad_key)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    TEST_DATA_T *testDataPtr = &testData;

    // Call function under test
    bool result = GKLIST_GetRef( &gkListHandle, gkListHandle.valueListCapacityElements, (char **) &testData );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(testDataPtr, &testData);
}


