//
// Copyright (C) 2021 by Morgan Camp
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///
/// @brief Unit test for circular buffer
///

//
// Project Includes
//
#include "CppUTest/TestHarness.h"

extern "C"
{
#include "GenericList.h"
}

//
// System Includes
//
// N/A

//
// #defines, consts
//
// N/A

//
// typedefs, enums, structs
//
typedef struct
{
    bool testBool;
    float testFloat;
    int testInt;
} TEST_DATA_T;

//
// Local variables
//
const size_t cbufferSize = 13;
DEFINE_GEN_LIST(gListHandle, TEST_DATA_T, 13);

//
// Function prototypes
//
// N/A

//
// Unit Tests
//
TEST_GROUP(group_GLIST_Set)
{
    TEST_SETUP()
    {
        gListHandle.values = &gListHandle_Arr[0];

        for( size_t i = 0; i < gListHandle.valueListCapacityElements; i++ )
        {
            gListHandle.values[i] = 0;
        }
    }
};


TEST_GROUP(group_GLIST_Copy)
{
    TEST_SETUP()
    {
        gListHandle.values = &gListHandle_Arr[0];

        for( size_t i = 0; i < gListHandle.valueListCapacityElements; i++ )
        {
            gListHandle.values[i] = 0;
        }
    }
};

TEST_GROUP(group_GLIST_GetRef)
{
    TEST_SETUP()
    {
        gListHandle.values = &gListHandle_Arr[0];

        for( size_t i = 0; i < gListHandle.valueListCapacityElements; i++ )
        {
            gListHandle.values[i] = 0;
        }
    }
};

TEST_GROUP(group_GLIST_Clear)
{
    TEST_SETUP()
    {
        gListHandle.values = &gListHandle_Arr[0];

        for( size_t i = 0; i < gListHandle.valueListCapacityElements; i++ )
        {
            gListHandle.values[i] = 0;
        }
    }
};

TEST(group_GLIST_Set, good_set_all)
{
    for( size_t i = 0; i < gListHandle.valueListCapacityElements; i++ )
    {
        // Setup data
        TEST_DATA_T testData = { true, (float) i, (int) i };

        // Call function under test
        bool result = GLIST_Set( &gListHandle, i, (char *) &testData );

        // Perform tests
        CHECK_TRUE(result);
    }
}

TEST(group_GLIST_Set, bad_listHandle)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t idxUsed = 0;

    // Call function under test
    bool result = GLIST_Set( NULL, idxUsed, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_Set, bad_listHandleValues)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t idxUsed = 0;
    gListHandle.values = NULL;

    // Call function under test
    bool result = GLIST_Set( &gListHandle, idxUsed, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_Set, bad_data)
{
    // Setup data
    size_t idxUsed = 0;

    // Call function under test
    bool result = GLIST_Set( &gListHandle, idxUsed, NULL );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_Set, bad_idx)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };

    // Call function under test
    bool result = GLIST_Set( &gListHandle, gListHandle.valueListCapacityElements, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_Copy, good_copy_all)
{
    // Setup data
    bool lastBool = true;
    for( size_t i = 0; i < gListHandle.valueListCapacityElements; i++ )
    {
        lastBool = !lastBool;
        TEST_DATA_T testData = { lastBool, (float) i, (int) i };

        // Call function under test
        (void) GLIST_Set( &gListHandle, i, (char *) &testData );
    }

    lastBool = true;
    for( size_t i = 0; i < gListHandle.valueListCapacityElements; i++ )
    {
        lastBool = !lastBool;
        TEST_DATA_T testData = { 0 };

        // Call function under test
        bool result = GLIST_Copy( &gListHandle, (size_t) i, (char *) &testData);

        // Perform tests
        CHECK_TRUE(result);
        CHECK_EQUAL(testData.testBool, lastBool);
        CHECK_EQUAL(testData.testFloat, (float) i);
        CHECK_EQUAL(testData.testInt, (int) i);
    }
}

TEST(group_GLIST_Copy, bad_listHandle)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t idxUsed = 12345;

    // Call function under test
    bool result = GLIST_Copy( NULL, idxUsed, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_Copy, bad_listHandleValues)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t idxUsed = 12345;
    gListHandle.values = NULL;

    // Call function under test
    bool result = GLIST_Copy( &gListHandle, idxUsed, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_Copy, bad_data)
{
    // Setup data
    size_t idxUsed = 12345;

    // Call function under test
    bool result = GLIST_Copy( &gListHandle, idxUsed, NULL );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_Copy, bad_idx)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };

    // Call function under test
    bool result = GLIST_Copy( &gListHandle, gListHandle.valueListCapacityElements, (char *) &testData );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_GetRef, good_get_ref_all)
{
    // Setup data
    bool lastBool = true;
    for( size_t i = 0; i < gListHandle.valueListCapacityElements; i++ )
    {
        lastBool = !lastBool;
        TEST_DATA_T testData = { lastBool, (float) i, (int) i };

        // Call function under test
        (void) GLIST_Set( &gListHandle, i, (char *) &testData );
    }

    lastBool = true;
    for( size_t i = 0; i < gListHandle.valueListCapacityElements; i++ )
    {
        lastBool = !lastBool;
        TEST_DATA_T testData = {0};
        TEST_DATA_T *testDataPtr = &testData;

        // Call function under test
        bool result = GLIST_GetRef( &gListHandle, (size_t) i, (char**) &testDataPtr );

        // Perform tests
        CHECK_TRUE(result);

        CHECK_EQUAL(testData.testBool, 0);
        CHECK_EQUAL(testData.testFloat, 0);
        CHECK_EQUAL(testData.testInt, 0);

        CHECK_EQUAL(testDataPtr->testBool, lastBool);
        CHECK_EQUAL(testDataPtr->testFloat, (float) i);
        CHECK_EQUAL(testDataPtr->testInt, (int) i);
    }
}

TEST(group_GLIST_GetRef, bad_listHandle)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    size_t idxUsed = 12345;
    TEST_DATA_T *testDataPtr = &testData;

    // Call function under test
    bool result = GLIST_GetRef( NULL, idxUsed, (char **) &testDataPtr );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(&testData, testDataPtr);
}

TEST(group_GLIST_GetRef, bad_listHandleValues)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    TEST_DATA_T *testDataPtr = &testData;
    size_t idxUsed = 12345;
    gListHandle.values = NULL;

    // Call function under test
    bool result = GLIST_GetRef( &gListHandle, idxUsed, (char **) &testDataPtr );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(testDataPtr, &testData);
}

TEST(group_GLIST_GetRef, bad_data)
{
    // Setup data
    size_t idxUsed = 12345;

    // Call function under test
    bool result = GLIST_GetRef( &gListHandle, idxUsed, NULL );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_GetRef, bad_idx)
{
    // Setup data
    TEST_DATA_T testData = { true, 1, 1 };
    TEST_DATA_T *testDataPtr = &testData;

    // Call function under test
    bool result = GLIST_GetRef( &gListHandle, gListHandle.valueListCapacityElements, (char **) &testData );

    // Perform tests
    CHECK_FALSE(result);
    CHECK_EQUAL(testDataPtr, &testData);
}

TEST(group_GLIST_Clear, good)
{
    // Setup data
    // N/A

    // Call function under test
    bool result = GLIST_Clear( &gListHandle );

    // Perform tests
    CHECK_TRUE(result);
    for( size_t i = 0; i < (gListHandle.valueListCapacityElements * gListHandle.valueElementSizeBytes); i++ )
    {
        CHECK_EQUAL(gListHandle.values[i], 0);
    }
}

TEST(group_GLIST_Clear, bad_listHandle)
{
    // Setup data
    // N/A

    // Call function under test
    bool result = GLIST_Clear( NULL );

    // Perform tests
    CHECK_FALSE(result);
}

TEST(group_GLIST_Clear, bad_listHandleValues)
{
    // Setup data
    gListHandle.values = NULL;

    // Call function under test
    bool result = GLIST_Clear( &gListHandle );

    // Perform tests
    CHECK_FALSE(result);
}
