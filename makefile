#Set this to @ to keep the makefile quiet
#SILENCE = @

#---- Outputs ----#
COMPONENT_NAME = Project
#Set this to @ to keep the makefile quiet
#SILENCE = @

#--- Inputs ----#
TEST_HOME = test

PROJECT_HOME_DIR = .

CPPUTEST_HOME = $(PROJECT_HOME_DIR)/cpputest

CPPFLAGS += -I$(CPPUTEST_HOME)/include

CPP_PLATFORM = Gcc

SRC_DIRS = \
    src\
    src/*

# to pick specific files (rather than directories) use this:    
SRC_FILES = 

TEST_SRC_DIRS = \
    $(TEST_HOME) \
    $(TEST_HOME)/*

MOCKS_SRC_DIRS = \
    $(TEST_HOME)/mocks

INCLUDE_DIRS =\
  .\
  $(PROJECT_HOME_DIR)/include/ \
  $(CPPUTEST_HOME)/include/ \
  $(CPPUTEST_HOME)/include/Platforms/Gcc

CPPUTEST_WARNINGFLAGS = -Wall -Werror -Wswitch-default 
CPPUTEST_WARNINGFLAGS += -Wconversion -Wswitch-enum 

CPPUTEST_USE_GCOV=Y
CPPUTEST_USE_VPATH=Y

include $(CPPUTEST_HOME)/build/MakefileWorker.mk

