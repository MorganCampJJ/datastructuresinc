//
// Copyright (C) 2021 by Morgan Camp
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without l> imitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///
/// @brief APIs to manage a circular buffer. The circular buffer is FIFO.
/// @TODO This data structure is not as efficient working with data elements larger than one char (byte)
///       For example, a 32-bit processor may handle copying a 32-bit element in one write cycle,
///       but this buffer takes 4 write cycles to do the same.
///       Update to efficiently write/read multi-byte data.

#ifndef __CIRCULAR_BUFFER__
#define __CIRCULAR_BUFFER__

//
// Library Includes
//
// N/A


//
// System Includes
//
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


//
// #defines, consts
//

// For use with definition of circular buffer.
typedef struct
{
    char *buffer;
    size_t capacity;
    size_t firstIdx;
    size_t lastIdx;
    size_t overrunCount;
    bool full;
} CBUF_T;

//
// The following can be used to statically define a circular buffer and its corresponding storage as follows:
//
// static char bufArrayName[bufSize] = {0};
// static CIRCULAR_BUFFER_T bufferName =
// {
//     .buffer = &bufArrayName[0],
//     .capacity = bufSize,
//     .lastIdx = 0,
//     .firstIdx = 0,
//     .overrunCount = 0,
//     .full = false
// };
#define DEFINE_CBUF( bufName, bufArrayName, bufSize ) \
static char bufArrayName[bufSize] = {0}; \
static CBUF_T bufName = \
{ \
    /*.buffer = */ &bufArrayName[0], \
    /*.capacity = */ bufSize, \
    /*.firstIdx = */ 0, \
    /*.lastIdx = */ 0, \
    /*.overrunCount  = */ 0, \
    /*.full  = */ false \
}



//
// typedefs, enums, structs
//
typedef CBUF_T* CBUF_Handle;


//
// Function prototypes
//
///
/// @brief Initialize a cbuffer.
///
/// @param[in] cbuf  The buffer handle.
///
/// @param[in] *cbufArr  The data container for buffer elements.
///
/// @param[in] bufferSize  The size of the data container.
///
/// @return  True if success, false otherwise.
///
bool CBUF_Init( CBUF_Handle cbuf, char *cbufArr, const size_t bufferSize );

///
/// @brief Empty a cbuffer.
///
/// @param[in] cbuf  The buffer handle.
///
/// @return  True if success, false otherwise.
///
bool CBUF_SetEmpty( CBUF_Handle cbuf );

///
/// @brief Get the number of bytes stored in the buffer.
///
/// @param[in] cbuf  The buffer handle.
///
/// @param[out] size  The number of bytes stored in the buffer.
///
/// @return  True if success, false otherwise.
///
bool CBUF_GetSize( CBUF_Handle cbuf, size_t* size );

///
/// @brief Get the number of bytes available to store in the buffer.
///
/// @param[in] cbuf  The buffer handle.
///
/// @param[out] size  The number of bytes available in the buffer.
///
/// @return  The number of bytes available in the buffer.
///
bool CBUF_GetAvailable( CBUF_Handle cbuf, size_t* size );

///
/// @brief Append multiple bytes to the end of the buffer. Appends valuesSize bytes, or none at all.
///
/// @param[in] cbuf  The buffer handle.
///
/// @param[in] values  The container of values to place in the buffer.
///
/// @param[in] valuesSize  The total number of bytes to place.
///
/// @return  True if successfully appends, false otherwise.
///
bool CBUF_Append( CBUF_Handle cbuf, char *values, const size_t valuesSize );

///
/// @brief Append multiple bytes to the end of the buffer, overwriting unread bytes on overrun.
///
/// @param[in] cbuf  The buffer handle.
///
/// @param[in] values  The container of values to place in the buffer.
///
/// @param[in] valuesSize  The total number of bytes to place.
///
/// @return  Number of bytes appended, up to byte capacity.
///
size_t CBUF_AppendForce( CBUF_Handle cbuf, char *values, size_t valuesSize );


///
/// @brief Remove multiple bytes, starting with the first. Removes valuesSize bytes, or none at all.
///
/// @param[in] cbuf  The buffer handle.
///
/// @param[out] values  The container to store data pulled from the buffer.
///
/// @param[in] valuesSize  The total number of bytes to remove.
///
/// @return  True if bytes removed successfully, false otherwise.
///
bool CBUF_Remove( CBUF_Handle cbuf, char* values, const size_t valuesSize );


///
/// @brief Remove multiple bytes, starting with the first. Removes as many as possible.
///
/// @param[in] cbuf  The buffer handle.
///
/// @param[out] values  The container to store data pulled from the buffer.
///
/// @param[in] valuesSize  The total number of bytes to remove.
///
/// @return  Number of bytes removed.
///
size_t CBUF_RemoveForce( CBUF_Handle cbuf, char* values, size_t valuesSize );


///
/// @brief Peek bytes from first at offset.
///
/// @param[in] cbuf  The buffer handle.
///
/// @param[out] value  The container to store data read from the buffer.
///
/// @param[in] valuesSize  The total number of bytes to read.
///
/// @param[in] offset  Offset from the front of the buffer.
///
/// @return  The number of bytes read from the buffer. Success
///          when return == valuesSize
///
size_t CBUF_Peek( CBUF_Handle cbuf, char *values, size_t valuesSize, const size_t offset );


#endif // __CIRCULAR_BUFFER__
