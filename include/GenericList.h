//
// Copyright (C) 2021 by Morgan Camp
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without l> imitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///
/// @brief APIs to manage a generic list of elements.
///
// Values are stored as byte arrays. Keys are fixed-size and values are fixed-size.
// The size of values are determined during the initialization of the Key Value Handler.
// Open spaces are stored in a separate array
// Examples of how data is stored in the idx value list:
//
// [Example - 4-byte-sized data value list, 2 elements:]
// Value list: |v1 b0|...|v1 b3|v2 b0|...|v2 b4| [Open space] |
// Keys function: kn = ( n * value_size ) = ( n * 4 )
// Values list, total elements: 2 (not full)
//
// [Example - 8-byte-sized data value list, 2 elements:]
// Value list, 8-byte data: |v1 b0|...|v1 b7|v2 b0|...|v2 b7| [Open space] |
// Keys function: kn = ( n * value_size ) = ( n * 8 )
// Values list, total elements: 2 (not full)

#ifndef __GENERIC_LIST__
#define __GENERIC_LIST__

//
// Library Includes
//


//
// System Includes
//
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


//
// #defines, consts
//
//
typedef struct
{
    // Data associated with the LinkedList.
    char *values; // space allocated for values.
    const size_t valueElementSizeBytes; // The size of each data element.
    const size_t valueListCapacityElements; // Total number of data elements allowed
} GEN_LIST_T;

//
// The following can be used to statically define a linked list and its corresponding storage as follows:
#define DEFINE_GEN_LIST(gHandleName, dataType, gLArrayLength) \
static char gHandleName##_Arr[sizeof(dataType) * gLArrayLength] = {0}; \
static GEN_LIST_T gHandleName = \
{ \
    .values = &gHandleName##_Arr[0], \
    .valueElementSizeBytes = sizeof(dataType), \
    .valueListCapacityElements = gLArrayLength, \
};


//
// typedefs, enums, structs
//
typedef GEN_LIST_T* GenList_Handle;

//
// Function prototypes
//
///
/// @brief Insert an element into the generic list.
///
/// @param[in] listHandle  The list handle
///
/// @param[in] idx  The idx for the value stored.
///
/// @param[in] data  The data container for the list element.
//             The length is assumed to be valueElementSizeBytes.
///
/// @return  True if set, false otherwise.
///
bool GLIST_Set( GenList_Handle listHandle, size_t idx, char* data );

///
/// @brief Get an element from the generic list.
///
/// @param[in] listHandle  The list handle
///
/// @param[in] idx  The idx to retrieve data from.
///
/// @param[out] data  The data container for the list element.
//              The length is assumed to be valueElementSizeBytes.
///
/// @return  True if data retrieved, false otherwise.
///
bool GLIST_Copy( GenList_Handle listHandle, size_t idx, char* data );

///
/// @brief Get reference of the location in list.
///
/// @param[in] listHandle  The list handle
///
/// @param[in] idx  The idx identifying the start of location.
///
/// @param[out] data  The data ptr to write the location of the idx's data.
///
/// @return  True if data retrieved, false otherwise.
///
bool GLIST_GetRef( GenList_Handle listHandle, size_t idx, char** data );

///
/// @brief Clear memory.
///
/// @param[in] listHandle  The list handle
///
/// @return  True if data retrieved, false otherwise.
///
bool GLIST_Clear( GenList_Handle listHandle );

#endif // __GENERIC_LIST__
