//
// Copyright (C) 2021 by Morgan Camp
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without l> imitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///
/// @brief APIs to manage a generic list of elements.
///
// Values are stored as byte arrays. Keys are fixed-size and values are fixed-size.
// The size of values are determined during the initialization of the Key Value Handler.
// Open spaces are stored in a separate array
// Examples of how data is stored in the key value list:
//
// [Example - 4-byte-sized data value list, 2 elements:]
// Value list: |v1 b0|...|v1 b3|v2 b0|...|v2 b4| [Open space] |
// Keys function: kn = ( n * value_size ) = ( n * 4 )
// Freed keys list: empty
// Values list, total elements: 2 (not full)
//
// [Example - 8-byte-sized data value list, 2 elements:]
// Value list, 8-byte data: |v1 b0|...|v1 b7|v2 b0|...|v2 b7| [Open space] |
// Keys function: kn = ( n * value_size ) = ( n * 8 )
// Freed keys list: empty
// Values list, total elements: 2 (not full)
//
// As elements are removed, old keys are re-used.

#ifndef __GENERIC_KEY_LIST__
#define __GENERIC_KEY_LIST__

//
// Library Includes
//
#include "CircularBuffer.h"


//
// System Includes
//
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


//
// #defines, consts
//
//
typedef struct
{
    // Data associated with the LinkedList.
    char *values; // space allocated for values.
    const size_t valueElementSizeBytes; // The size of each data element.
    const size_t valueListCapacityElements; // Total number of data elements allowed
    size_t totalElementsStored; // Total number of data elements stored

    // Open Keys
    CBUF_Handle freedKeys; // Stores locations in data where there is storage space.
    bool *keyUsedLUT; // Quick lookup to determine if key slot is open
} GEN_KEY_LIST_T;

//
// The following can be used to statically define a linked list and its corresponding storage as follows:
#define DEFINE_GEN_KEY_LIST(gkHandleName, dataType, gkLArrayLength) \
static char gkHandleName##_Arr[sizeof(dataType) * gkLArrayLength] = {0}; \
static bool gkHandleName##_KeyUsedLUT[gkLArrayLength] = {0}; \
DEFINE_CBUF(gkHandleName##_Cbuf, gkHandleName##_CbufArr, (gkLArrayLength * sizeof(size_t))); \
static GEN_KEY_LIST_T gkHandleName = \
{ \
    .values = &gkHandleName##_Arr[0], \
    .valueElementSizeBytes = sizeof(dataType), \
    .valueListCapacityElements = gkLArrayLength, \
    .totalElementsStored = 0, \
    .freedKeys = &gkHandleName##_Cbuf, \
    .keyUsedLUT = &gkHandleName##_KeyUsedLUT[0], \
};


//
// typedefs, enums, structs
//
typedef GEN_KEY_LIST_T* GenList_Handle;


//
// Function prototypes
//
///
/// @brief Insert an element into the generic list.
///
/// @param[in] listHandle  The list handle
///
/// @param[in] data  The data container for the list element.
//             The length is assumed to be valueElementSizeBytes.
///
/// @param[out] keyUsed  The key for the value stored, which can be used for later access.
///
/// @return  True if inserted, false otherwise.
///
bool GKLIST_Insert( GenList_Handle listHandle, char* data, size_t *keyUsed );

///
/// @brief Remove an element from the generic list.
///
/// @param[in] listHandle  The list handle
///
/// @param[in] key  The key where data is removed from.
///
/// @return  True if data removed, false otherwise.
///
bool GKLIST_Remove( GenList_Handle listHandle, size_t key );

///
/// @brief Get an element from the generic list.
///
/// @param[in] listHandle  The list handle
///
/// @param[in] key  The key to retrieve data from.
///
/// @param[out] data  The data container for the list element.
//              The length is assumed to be valueElementSizeBytes.
///
/// @return  True if data retrieved, false otherwise.
///
bool GKLIST_Copy( GenList_Handle listHandle, size_t key, char* data );

///
/// @brief Get reference of the location in list.
///
/// @param[in] listHandle  The list handle
///
/// @param[in] key  The key identifying the start of location.
///
/// @param[out] data  The data ptr to write the location of the key's data.
///
/// @return  True if data retrieved, false otherwise.
///
bool GKLIST_GetRef( GenList_Handle listHandle, size_t key, char** data );

#endif // __GENERIC_KEY_LIST__
