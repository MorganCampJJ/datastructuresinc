//
// Copyright (C) 2021 by Morgan Camp
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without l> imitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//
// Library Includes
//
#include "CircularBuffer.h"

//
// System Includes
//

//
// Library Includes
//
// N/A


//
// System Includes
//
#include <assert.h>
#include <string.h>


//
// #defines, consts
//
#define CBUF_ERR_RET(condition) if(condition) { return false; }
#define CBUF_ERR_RET_SIZE(condition) if(condition) { return 0; }


//
// typedefs, enums, structs
//
// N/A


//
// Local variables
//
// N/A


//
// Function prototypes
//
static size_t mCBUF_Append( CBUF_Handle cbuf, char *values, size_t valuesSize );
static size_t mCBUF_Remove( CBUF_Handle cbuf, char *values, size_t valuesSize );
static size_t mCBUF_GetIndexAtOffset( size_t capacity, size_t startIdx, size_t offset );


//
// API Definitions
//
bool CBUF_Init( CBUF_Handle cbuf, char *cbufArr, const size_t bufferSize )
{
    CBUF_ERR_RET(NULL == cbuf);
    CBUF_ERR_RET(NULL == cbufArr);
    CBUF_ERR_RET(bufferSize <= 0);

    cbuf->buffer = cbufArr;
    cbuf->capacity = bufferSize;
    cbuf->firstIdx = 0;
    cbuf->lastIdx = 0;
    cbuf->overrunCount = 0;
    cbuf->full = false;

    return true;
}


bool CBUF_SetEmpty( CBUF_Handle cbuf )
{
    CBUF_ERR_RET(NULL == cbuf);

    cbuf->firstIdx = 0;
    cbuf->lastIdx = 0;
    cbuf->full = false;

    return true;
}


bool CBUF_GetSize( CBUF_Handle cbuf, size_t* size )
{
    CBUF_ERR_RET(NULL == cbuf);
    CBUF_ERR_RET(NULL == size);

    *size = cbuf->capacity;

    if( false == cbuf->full )
    {
        if( cbuf->lastIdx >= cbuf->firstIdx )
        {
            *size = cbuf->lastIdx - cbuf->firstIdx;
        }
        else
        {
            *size = cbuf->capacity + cbuf->lastIdx - cbuf->firstIdx;
        }
    }

    return true;
}


bool CBUF_GetAvailable( CBUF_Handle cbuf, size_t* size )
{
    CBUF_ERR_RET(NULL == cbuf);

    bool success = CBUF_GetSize(cbuf, size);
    if( true == success )
    {
        *size = ( cbuf->capacity - *size );
    }

    // total allowed elements - total elements
    return success;
}


bool CBUF_Append( CBUF_Handle cbuf, char* values, const size_t appendCount )
{
    CBUF_ERR_RET(0 == appendCount);

    size_t remaining = 0;
    bool success = CBUF_GetAvailable(cbuf, &remaining);
    success = success && ( remaining >= appendCount );

    if( true == success )
    {
        success = (appendCount == mCBUF_Append(cbuf, values, appendCount));
    }

    return success;
}


size_t CBUF_AppendForce( CBUF_Handle cbuf, char *values, size_t appendCount )
{
    return mCBUF_Append(cbuf, values, appendCount);;
}


bool CBUF_Remove( CBUF_Handle cbuf, char* values, const size_t removeCount )
{
    CBUF_ERR_RET_SIZE(0 == removeCount);

    size_t size = 0;
    bool success = CBUF_GetSize(cbuf, &size);
    success = success && ( size >= removeCount );

    if( true == success )
    {
        success = (removeCount == mCBUF_Remove(cbuf, values, removeCount));
    }

    return success;
}


size_t CBUF_RemoveForce( CBUF_Handle cbuf, char* values, size_t removeCount )
{
    size_t size = 0;
    bool success = CBUF_GetSize(cbuf, &size);

    if( size < removeCount )
    {
        removeCount = size;
    }

    if( true == success )
    {
        removeCount = mCBUF_Remove(cbuf, values, removeCount);
    }
    else
    {
        removeCount = 0;
    }

    return removeCount;
}


size_t CBUF_Peek( CBUF_Handle cbuf, char *values, size_t valuesSize, const size_t offset )
{
    CBUF_ERR_RET_SIZE(NULL == cbuf);
    CBUF_ERR_RET_SIZE(NULL == cbuf->buffer);
    CBUF_ERR_RET_SIZE(NULL == values);
    CBUF_ERR_RET_SIZE(0 == valuesSize);
    CBUF_ERR_RET_SIZE(offset >= cbuf->capacity);

    size_t size = 0;
    bool success = CBUF_GetSize(cbuf, &size);

    if(( true == success ) && ( size > 0 ))
    {
        // Can't peek more than size of buffer.
        if(( valuesSize + offset ) > size )
        {
            valuesSize = size - offset;
        }

        size_t startIdx = mCBUF_GetIndexAtOffset( cbuf->capacity, cbuf->firstIdx, offset );
        size_t endIdx = mCBUF_GetIndexAtOffset( cbuf->capacity, cbuf->firstIdx, (offset + valuesSize));

        // Check for overlap.
        if( startIdx >= endIdx )
        {
            size_t sizeFromArrayEnd = cbuf->capacity - startIdx;

            (void) memcpy(&values[0], &cbuf->buffer[startIdx], sizeFromArrayEnd);
            (void) memcpy(&values[sizeFromArrayEnd], &cbuf->buffer[0], (valuesSize - sizeFromArrayEnd));
        }
        else
        {
            (void) memcpy(&values[0], &cbuf->buffer[startIdx], valuesSize);
        }
    }
    else
    {
        valuesSize = 0;
    }

    return valuesSize;
}


//
// Local Function Definitions
//
static size_t mCBUF_Append( CBUF_Handle cbuf, char *values, size_t valuesSize )
{
    CBUF_ERR_RET_SIZE(NULL == cbuf);
    CBUF_ERR_RET_SIZE(NULL == cbuf->buffer);
    CBUF_ERR_RET_SIZE(NULL == values);
    CBUF_ERR_RET_SIZE(0 == valuesSize);

    size_t nextLastIdx = 0;

    if( valuesSize > cbuf->capacity )
    {
        valuesSize = cbuf->capacity;
    }
    nextLastIdx = cbuf->lastIdx + valuesSize;

    // Check for overlap.
    if( nextLastIdx > cbuf->capacity )
    {
        size_t bytesToEndOfArr = cbuf->capacity - cbuf->lastIdx;
        nextLastIdx = valuesSize - bytesToEndOfArr;

        (void) memcpy(&cbuf->buffer[cbuf->lastIdx], &values[0], bytesToEndOfArr);
        (void) memcpy(&cbuf->buffer[0], &values[nextLastIdx], nextLastIdx);
    }
    else
    {
        nextLastIdx = (nextLastIdx == cbuf->capacity) ? 0 : nextLastIdx;
        (void) memcpy(&cbuf->buffer[cbuf->lastIdx], &values[0], valuesSize);
    }
    cbuf->lastIdx = nextLastIdx;
    cbuf->full = (cbuf->lastIdx == cbuf->firstIdx);

    return valuesSize;
}


static size_t mCBUF_Remove( CBUF_Handle cbuf, char *values, size_t valuesSize )
{
    CBUF_ERR_RET_SIZE(NULL == cbuf);
    CBUF_ERR_RET_SIZE(NULL == cbuf->buffer);
    CBUF_ERR_RET_SIZE(NULL == values);
    CBUF_ERR_RET_SIZE(0 == valuesSize);

    size_t startIdx = mCBUF_GetIndexAtOffset( cbuf->capacity, cbuf->firstIdx, 0 );
    size_t endIdx = mCBUF_GetIndexAtOffset( cbuf->capacity, cbuf->firstIdx, valuesSize);

    // Check for overlap.
    if( startIdx >= endIdx )
    {
        size_t sizeFromArrayEnd = cbuf->capacity - startIdx;

        (void) memcpy(&values[0], &cbuf->buffer[startIdx], sizeFromArrayEnd);
        (void) memcpy(&values[sizeFromArrayEnd], &cbuf->buffer[0], (valuesSize - sizeFromArrayEnd));
    }
    else
    {
        (void) memcpy(&values[0], &cbuf->buffer[startIdx], valuesSize);
    }

    cbuf->firstIdx = endIdx;
    cbuf->full = false;

    return valuesSize;
}

static size_t mCBUF_GetIndexAtOffset( size_t capacity, size_t startIdx, size_t offset )
{
    size_t index = startIdx + offset;

    if( index >= capacity )
    {
        index = index - capacity;
    }
    return index;
}

