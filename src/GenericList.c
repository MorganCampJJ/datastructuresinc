//
// Copyright (C) 2021 by Morgan Camp
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without l> imitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//
// Library Includes
//
#include "GenericList.h"

//
// System Includes
//

//
// Library Includes
//
// N/A


//
// System Includes
//
#include <string.h>


//
// #defines, consts
//
#define GLIST_ERR_RET(condition) if(condition) { return false; }


//
// typedefs, enums, structs
//
// N/A


//
// Local variables
//
// N/A


//
// Function prototypes
//
// N/A


//
// APIs Definitions
//
bool GLIST_Set( GenList_Handle listHandle, size_t idx, char* data )
{
    GLIST_ERR_RET( NULL == listHandle );
    GLIST_ERR_RET( NULL == listHandle->values );
    GLIST_ERR_RET( NULL == data );
    GLIST_ERR_RET( idx >= listHandle->valueListCapacityElements);

    (void) memcpy(&listHandle->values[idx * listHandle->valueElementSizeBytes],
                  &data[0],
                  listHandle->valueElementSizeBytes);

    return true;
}


bool GLIST_Copy( GenList_Handle listHandle, size_t idx, char* data )
{
    GLIST_ERR_RET( NULL == listHandle );
    GLIST_ERR_RET( NULL == listHandle->values );
    GLIST_ERR_RET( NULL == data );
    GLIST_ERR_RET( idx >= listHandle->valueListCapacityElements );

    // Copy data from slot
    (void) memcpy( data,
                   &listHandle->values[idx * listHandle->valueElementSizeBytes],
                   listHandle->valueElementSizeBytes);

    return true;
}


bool GLIST_GetRef( GenList_Handle listHandle, size_t idx, char** data )
{
    GLIST_ERR_RET( NULL == listHandle );
    GLIST_ERR_RET( NULL == listHandle->values );
    GLIST_ERR_RET( NULL == data );
    GLIST_ERR_RET( idx >= listHandle->valueListCapacityElements );

    // Get a reference of the memory region at specified index.
    *data = &listHandle->values[idx * listHandle->valueElementSizeBytes];

    return true;
}


bool GLIST_Clear( GenList_Handle listHandle )
{
    GLIST_ERR_RET( NULL == listHandle );
    GLIST_ERR_RET( NULL == listHandle->values );

    // Set all data to 0.
    memset(&listHandle->values[0],
            0,
            listHandle->valueListCapacityElements * listHandle->valueElementSizeBytes);

    return true;
}


//
// Local Function Definitions
//
// N/A


