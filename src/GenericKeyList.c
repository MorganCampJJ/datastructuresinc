//
// Copyright (C) 2021 by Morgan Camp
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without l> imitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//
// Library Includes
//
#include "GenericKeyList.h"

//
// System Includes
//

//
// Library Includes
//
// N/A


//
// System Includes
//
#include <string.h>


//
// #defines, consts
//
#define GKLIST_ERR_RET(condition) if(condition) { return false; }


//
// typedefs, enums, structs
//
// N/A


//
// Local variables
//
// N/A


//
// Function prototypes
//
// N/A


//
// APIs Definitions
//
bool GKLIST_Insert( GenKeyList_Handle listHandle, char* data, size_t *keyUsed )
{
    GKLIST_ERR_RET( NULL == listHandle );
    GKLIST_ERR_RET( NULL == listHandle->values );
    GKLIST_ERR_RET( NULL == listHandle->keyUsedLUT );
    GKLIST_ERR_RET( NULL == keyUsed );
    GKLIST_ERR_RET( NULL == data );
    GKLIST_ERR_RET( listHandle->totalElementsStored > listHandle->valueListCapacityElements );

    size_t cbufSize = 0;
    bool success = CBUF_GetSize( listHandle->freedKeys, &cbufSize );

    if( true == success )
    {
        *keyUsed = listHandle->totalElementsStored + cbufSize;

        if( *keyUsed >= listHandle->valueListCapacityElements )
        {
            // If at least valueListCapacityElements have been stored previously, start using freedKeys list
            // to determine where to store the next element.
            success = CBUF_Remove( listHandle->freedKeys, (char *) keyUsed, sizeof( *keyUsed ) );
        }

        if( true == success)
        {
            (void) memcpy(&listHandle->values[*keyUsed * listHandle->valueElementSizeBytes],
                          &data[0],
                          listHandle->valueElementSizeBytes);
            listHandle->keyUsedLUT[*keyUsed] = true;
            listHandle->totalElementsStored++;
        }
    }

    return success;
}


bool GKLIST_Remove( GenKeyList_Handle listHandle, size_t key )
{
    GKLIST_ERR_RET( NULL == listHandle );
    GKLIST_ERR_RET( NULL == listHandle->keyUsedLUT );
    GKLIST_ERR_RET( key >= listHandle->valueListCapacityElements );

    bool success = false;

    if( true == listHandle->keyUsedLUT[key] )
    {
        success = CBUF_Append( listHandle->freedKeys, (char *) &key, sizeof( key ) );

        if( true == success )
        {
            listHandle->keyUsedLUT[key] = false;
            listHandle->totalElementsStored--;
        }
    }

    return success;
}


bool GKLIST_Copy( GenKeyList_Handle listHandle, size_t key, char* data )
{
    GKLIST_ERR_RET( NULL == listHandle );
    GKLIST_ERR_RET( NULL == listHandle->values );
    GKLIST_ERR_RET( NULL == listHandle->keyUsedLUT );
    GKLIST_ERR_RET( NULL == data );
    GKLIST_ERR_RET( key >= listHandle->valueListCapacityElements );

    bool success = false;

    if( true == listHandle->keyUsedLUT[key] )
    {
        // Slot contains data. Copy data
        (void) memcpy( data,
                       &listHandle->values[key * listHandle->valueElementSizeBytes],
                       listHandle->valueElementSizeBytes);
        success = true;
    }

    return success;
}


bool GKLIST_GetRef( GenKeyList_Handle listHandle, size_t key, char** data )
{
    GKLIST_ERR_RET( NULL == listHandle );
    GKLIST_ERR_RET( NULL == listHandle->values );
    GKLIST_ERR_RET( NULL == listHandle->keyUsedLUT );
    GKLIST_ERR_RET( NULL == data );
    GKLIST_ERR_RET( key >= listHandle->valueListCapacityElements );

    bool success = false;

    if( true == listHandle->keyUsedLUT[key] )
    {
        // Slot contains data. Copy data
        *data = &listHandle->values[key * listHandle->valueElementSizeBytes];
        success = true;
    }

    return success;
}


//
// Local Function Definitions
//
// N/A


